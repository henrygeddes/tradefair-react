
module.exports = {
	entry: './js/app.js',
	output: {
		filename: 'bundle.js',
		path: __dirname.substr(0, __dirname.lastIndexOf('/')) + '/public/js'
	},

	module: {
		loaders: [
			{
				test: /\.js?$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				query: {
					presets: ['react']
				}
			},
			{
				test: /\.scss$/,
				loaders: ['style', 'css', 'sass']
			}
		]
	},
	
	resolve: {
		alias: {
			jquery: __dirname + '/js/utils/jquery.min.js'
		}
	}
};
//Maybe look at react-hot-loader for live component/page refresh (without losing state)
