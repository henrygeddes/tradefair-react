//Requirements
require('../css/main.scss');
require('expose?$!expose?jQuery!jquery');

var React = require('react');
var ReactDom = require('react-dom');

//Components
var Router = require('./components/router');

//Create the app container
$("body").prepend('<div id="app"></div>'); //create our app container

//Create the app
var App = React.createClass({
	render(){
		return (
			<div>
				<Router className='router' />	
			</div>
		)
	}
});

ReactDom.render(
	<App />,
	$("#app")[0]
);
