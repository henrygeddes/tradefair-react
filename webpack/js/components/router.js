var React = require('react');

//Routes
var Home = React.createClass({
	render: function(){
		return(
			<div className='home'>
				<p>Test</p>
			</div>
		)
	}
});

var Router = React.createClass({
	routes: {
		'home': Home
	},

	getInitialState: function(){
		return{
			currentView: 'Home'
		};
	},

	render: function(){
		return(
			<this.state.currentView />
		)
	},

});

module.exports = Router;
