var React = require('react');

var MenuCategoryItem = React.createClass({
	render(){
		return(
			<div className="menu__category"></div>
		);
	}
});

var MenuItem = React.createClass({
	render(){
		return(
			<div className="menu__item">{this.props.text}</div>
		);
	}
});

var Menu = React.createClass({
	render(){
		return(
			<div className={"menu " + this.props.extraClasses}>
				<MenuItem link="/" text="Browse" />
				<MenuItem link="/" text="Browse" />
				<MenuItem link="/" text="Browse" />
				<MenuItem link="/" text="Browse" />
			</div>
		);
	}
});

module.exports = Menu;
