var React = require('react');
var Menu = require('./menu');

var Header = React.createClass({
	render(){
		return(
			<div className="header container">
				<div className="row">
					<div className="col-md-5">
						<img src="/images/logo.png" className="img img-responsive logo"/>
					</div>

					<div className="col-md-7">
						<Menu />
					</div>
				</div>
			</div>
		);
	}
});
