//External requires
var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var logger = require('morgan');

//Create our app and debug logger
var app = express();
var debug = require('debug')('tf-orm-express:server');


//Setup our app
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/', express.static(__dirname + '/public'));


//Get api routes
var UserRoutes = require('./routes/usersRoutes');
var ListingRoutes = require('./routes/listingRoutes');
var MotorRoutes = require('./routes/motorRoutes');

//API routing
var apiRoute = ("/api");
app.use(apiRoute + '/users', UserRoutes);
app.use(apiRoute + '/listing', ListingRoutes);
app.use(apiRoute + '/motor', MotorRoutes);

//Error routes
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

//Dev logging
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500)
      .json({
        message: 'An error occured',
        error: err
      });
  });
}

//Production logging
app.use(function(err, req, res, next) {
  res.status(err.status || 500)
    .json({
      message: err.message,
      error: {}
    });
});

module.exports = app;

/*
var app = express();

// view engine setup
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//app.use(cookieParser());
//app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
*/
