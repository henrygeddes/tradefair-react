"use strict";

module.exports = function(sequelize, DataTypes){
	var Question = sequelize.define('Question', {
		question: DataTypes.STRING,
		answer: DataTypes.STRING
		}, {
		classMethods: {
			associate: function(models){
				Question.belongsTo(models.Listing)
			}
		}
	});

	return Question;
};
