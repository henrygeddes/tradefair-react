"use strict";

module.exports = function(sequelize, DataTypes){
	var Listing = sequelize.define('Listing', {
		listing: {
			type: DataTypes.STRING,
			validate: { isAlphanumeric: true }
		},

		startDate: {
			type:  DataTypes.DATE,
			validate: { isDate: true }
		},

		endDate: {
			type:  DataTypes.DATE,
			validate: { isDate: true }
		},

		views: {
			type		: DataTypes.INTEGER,
			defaultValue: 0,
			validate: { isNumeric: true }
		},

		startBid: {
			type: DataTypes.DECIMAL,
			validate: { isNumeric: true }
		},

		leadingBid: {
			type: DataTypes.DECIMAL,
			validate: { isNumeric: true }
		},

		buyNow: {
			type: DataTypes.DECIMAL,
			validate: { isNumeric: true }
		},

		reserve: {
			type: DataTypes.DECIMAL,
			validate: { isNumeric: true }
		}
	}, {
		classMethods: {
			associate: function(models){
				Listing.belongsTo(models.User)
			}
		}
	});

	return Listing;
};
