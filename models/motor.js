"use strict";

module.exports = function(sequelize, DataTypes){
	var Motor = sequelize.define('Motor', {
		make			: DataTypes.STRING,
		model			: DataTypes.STRING,
		modelDetail 	: DataTypes.STRING,
		year			: DataTypes.INTEGER,
		kms				: DataTypes.INTEGER,
		colors			: DataTypes.STRING,
		doors			: DataTypes.INTEGER,
		bodyStyle		: DataTypes.STRING,
		fuelType		: DataTypes.STRING,
		cylinders		: DataTypes.STRING,
		engineSize		: DataTypes.INTEGER,
		transmission	: DataTypes.STRING,
		fourWD			: DataTypes.BOOLEAN,
		numberOfOwners	: DataTypes.INTEGER,
		importHistory	: DataTypes.STRING,
		regoExpires		: DataTypes.DATE,
		wofExpires		: DataTypes.DATE,
		numberPlate		: DataTypes.STRING,
		description		: DataTypes.TEXT

		}, {
		classMethods: {
			associate: function(models){
				Motor.belongsTo(models.Listing)
			}
		}
	});

	return Motor;
};
