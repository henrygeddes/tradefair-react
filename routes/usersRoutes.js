var express = require('express');
var router = express.Router();

var models = require('../models');

/* GET users listing. */
router.get('/', function(req, res, next) {
  models.User.findAll().success(function(users, err){
    res.json({users: users});
  });
});

module.exports = router;
