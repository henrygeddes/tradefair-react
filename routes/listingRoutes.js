var util = require('util');
var express = require('express');
var router = express.Router();
var validator = require('validator');
var sanitizer = require('sanitizer');

var models = require('../models');

/* GET Listings  */
router.get('/', function(req, res, next) {
  models.Listing.findAll().success(function(rows, err){

    res.json({
		data: rows,
		err	: err
	});

  });
});

router.post('/', function(req, res, next){
	var listing = {
		'listing'	: req.body.listing,
		'startDate'	: req.body.startDate,
		'endDate'	: req.body.endDate, 
		'views'		: 0,
		'startBid'	: req.body.startBid,
		'leadingBid': 0,
		'buyNow'	: req.body.buyNow,
		'reserve'	: req.body.reserve
	};

	res.json({
		'message' : 'hey'
	});
});

module.exports = router;
