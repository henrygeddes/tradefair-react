var express = require('express');
var router = express.Router();

var models = require('../models');

/* GET Listings  */
router.get('/', function(req, res, next) {
  models.Motor.findAll().success(function(rows, err){

    res.json({
		data: rows,
		err	: err
	});

  });
});

//use req.value to create motor object, then models.Motor.create (info in sequelize docs)
router.post('/', function(req, res){
	console.log(req.body);

	res.json({
		data: "test",
		body: req.body
	});
});

module.exports = router;
